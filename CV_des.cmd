#setdep @node|LGADStrip2D@


device DIODE {
File {
    Grid       = "n@node|LGADStrip2D@_msh.tdr"
    Current    = "@plot@"
    Plot       = "@tdrdat@"
    parameter = "@parameter@"
}



Electrode {
	!(
	for {set i 1} {$i<=@<NumStripes+2>@} {incr i} {
					puts "\{name = \"cathode${i}\"   voltage = 0.0   eRecVelocity=1e7 hRecVelocity=1e7 \}"	
	}
	)!

	{name = "anode"   voltage = 0.0    	eRecVelocity=1e7 hRecVelocity=1e7 }	
		
}


Physics { 
	##AreaFactor=1e8
	##Fermi
 }
        
 Physics (material="Silicon") { 

 	Mobility( 
		DopingDep(Unibo)
            	HighFieldsat(GradQuasiFermi)
 	  	)
  	Recombination( 
		 SRH(DopingDep TempDep)
		 Auger
  		##hAvalanche(UniBo) eAvalanche(UniBo)  		
  		hAvalanche eAvalanche 		
		)
  	EffectiveIntrinsicDensity(OldSlotboom)
  	
 	Traps(
  			(
  			name="state1" acceptor conc=@<fluence*1.613>@
  			Level FromConductionBand	EnergyMid=0.42
  			eXsection=2E-15  hXsection=2E-14
			##eJfactor=1.0 hJfactor=1.0
			)
  			(
  			name="state2" acceptor conc=@<fluence*100.0>@
  			Level FromConductionBand	EnergyMid=0.46
  			eXsection=5E-15  hXsection=5E-14
			##eJfactor=1.0 hJfactor=1.0
			)
  			(
  			name="state3" donor conc=@<fluence*0.9>@
  			Level FromValenceBand	EnergyMid=0.36
  			eXsection=2.5E-14  hXsection=2.5E-15
			##eJfactor=1.0 hJfactor=1.0
			)
		)



} 




CurrentPlot {
eLifeTime(Maximum(material="Silicon"))
hLifeTime(Maximum(material="Silicon"))
eAvalanche(Maximum(material="Silicon"))
hAvalanche(Maximum(material="Silicon"))
}
}


Math {

	##CDensityMin=1e-100
	Extrapolate
    Derivatives
  Avalderivatives
  Digits=5
  Notdamped=1000
  Iterations=20
  RelerrControl
  ErrRef(electron)=1e6
  ErrRef(hole)=1e6
  RhsMin=1e-15
  
    eMobilityAveraging=ElementEdge       
  hMobilityAveraging=ElementEdge       
  ParallelToInterfaceInBoundaryLayer(-ExternalBoundary)

  Method=Blocked
  ##SubMethod=ILS (set=1)
  SubMethod=Pardiso
  ACMethod=Blocked 
  ##ACSubMethod=ILS (set=2)
  ACSubMethod=Pardiso
 
  
  ILSrc = "
           set(1) { 
                iterative( gmres(100), tolrel=1e-8, tolunprec=1e-4, tolabs=0, maxit=200 ); 
                preconditioning( ilut(0.0001,-1), left ); 
                ordering( symmetric=nd, nonsymmetric=mpsilst ); 
                options( compact=yes, verbose=0, refineresidual=0 ); 
           };
           
           set(2) { 
                iterative( gmres(100), tolrel=1e-8, tolunprec=1e-4, tolabs=0, maxit=200 ); 
                preconditioning( ilut(1e-8,-1), left ); 
                ordering( symmetric=nd, nonsymmetric=mpsilst ); 
                options( compact=yes, verbose=0, refineresidual=0 ); 
           };

           
     "

  number_of_threads=4

}

system {
	DIODE D1 ("anode"=a
	!(
	for {set i 1} {$i<=@NumStripes@} {incr i} {
					puts "\"cathode${i}\"=c${i}"   	
	}
	)!
	) {}
	
	Vsource_pset va (a gnd) {dc=0}

	!(
	for {set i 1} {$i<=@NumStripes@} {incr i} {
					puts "Vsource_pset vc${i} (c${i} gnd) \{dc=0\}"   	
	}
	)!
	
	
	set(gnd=0)
}


Solve {

   Poisson
  Coupled { Poisson Electron Hole }

              	
   Quasistationary (  		DoZero
   			MaxStep=0.1  MinStep=5e-6 InitialStep=1e-2
			Increment=1.6 Decrement=4.0
                  		Goal { Parameter="va"."dc" Voltage=0.6 } 
                 		)
                  { Coupled {  Poisson Electron Hole } } 
      
 NewCurrent="CV_"    

    Quasistationary (  		DoZero
   			MaxStep=0.02  MinStep=5e-6 InitialStep=1e-4
			Increment=1.6 Decrement=4.0
                  		Goal { Parameter="va"."dc" Voltage=-300 } 
                 		)
                  { 
                  	  ACCoupled (
                  	  	StartFrequency=1.0 EndFrequency=1.0
                  	  	NumberOfPoints=1 Decade
                  	  		!(
                  	  		set nodelist "a"
                  	  		set sourcelist "va"
                  	  		for {set i 1} {$i<=@NumStripes@} {incr i} {
                  	  				set nodelist "$nodelist c${i}"
                  	  				set sourcelist "$sourcelist vc${i}"   	
                  	  		}
                  	  		puts "Node($nodelist) Exclude($sourcelist)"   	
                  	  		)!

                  	  	) {  Poisson Electron Hole } 
                  } 


}
   
file {
	ACExtract = "@acplot@"
    output = "@log@"
}

Plot {
	
        Current/Vector	
	eCurrent/Vector
	hCurrent/Vector
	eDensity
	hDensity
	ElectricField/Vector
	Potential
	CurrentPotential
  	DopingConcentration	
	eMobility
	hMobility
	DonorConcentration
	AcceptorConcentration
 	AvalancheGeneration
 	
 	eAvalanche hAvalanche
 	eLifeTime hLifeTime


}

