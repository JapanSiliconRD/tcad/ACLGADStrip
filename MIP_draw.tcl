#MIP_draw.tcl
#MAKO: Output from Tcl commands history.
set node "n15"

load_file MIP_${node}_des.plt
create_plot -1d
select_plots {Plot_1}
#-> Plot_1
#-> Plot_1
#-> MIP_n15_des
#create_curve -plot Plot_1 -dataset MIP_${node}_des -axisX time -axisY {anode TotalCurrent}
#create_curve -plot Plot_1 -dataset MIP_${node}_des -axisX time -axisY {cathode1 TotalCurrent}
create_curve -plot Plot_1 -dataset MIP_${node}_des -axisX time -axisY {cathode2 TotalCurrent}
create_curve -plot Plot_1 -dataset MIP_${node}_des -axisX time -axisY {cathode3 TotalCurrent}
create_curve -plot Plot_1 -dataset MIP_${node}_des -axisX time -axisY {cathode4 TotalCurrent}
create_curve -plot Plot_1 -dataset MIP_${node}_des -axisX time -axisY {cathode5 TotalCurrent}
create_curve -plot Plot_1 -dataset MIP_${node}_des -axisX time -axisY {cathode6 TotalCurrent}
#create_curve -plot Plot_1 -dataset MIP_${node}_des -axisX time -axisY {cathode7 TotalCurrent}
set_plot_prop -plot {Plot_1} -hide_title
set_axis_prop -plot Plot_1 -axis x -min 4e-09 -min_fixed
set_axis_prop -plot Plot_1 -axis x -max 1e-08 -max_fixed
#-> 0

set_legend_prop -plot Plot_1 -position {0.6 0.8}
