

(sde:clear)

(define W 50)

(define Lstripe_pitch 80.0) ;; strip pitch
(define LNplus_stripe 30.0) ;; width of n+
(define LGR_pitch 30.0) ;; GR pitch
(define LNplus_GR 15.0) ;; GR n+ width
;;(define gap (- 10 (* 0.5 (- LGR_pitch LNplus_GR)))) 
(define gap 80.0)  ;; gap between last strip to GL

(define p_layer_Peak		1.9)
(define p_layer_Depth		2.7)


(define NSTRIPE @NumStripes@)
(define NGR 3)
(define Xj @nplus_dep@) ;;  n+ depth

(define EXT 100.0)

(define L (+ (* NSTRIPE Lstripe_pitch) (* 1.0 gap) (* NGR LGR_pitch) EXT))

(define Plow 1e12)
(define Nca0 0)
(define Nca @nplus_dope@) ;; 1e19
(define Pan 1e19)
(define pspray 5e16)
(define whi 0.1)
(define Lhi 50)
(define p_well_doping_concentration @pplus_dope@) ; 3e16
(define al_dep 0.3)
(sdegeo:create-rectangle (position (- 0 L) 0 0) (position L W 0) "Silicon" "RSub")

(define end-active 0)
(define poslist (list))

;;(define dirX 0)
;;(define dirY 1)

;;(sdedr:define-refeval-window "RefEval_Pplus" "Line"  
;;        (position (- 0 (+ L 10)) W 0) 
;;        (position (+ L 10) W 0)
;;)
;;
;;(sdedr:define-gaussian-profile "DopAnode" "BoronActiveConcentration" "PeakPos" 0  "PeakVal" Pan "ValueAtDepth" Plow "Depth" 5 "Gauss"  "Factor" 0.8)
;;(sdedr:define-analytical-profile-placement "Place_anode" "DopAnode" "RefEval_anode" "Both" "NoReplace" "Eval")



(if (eq? (modulo NSTRIPE 2) 0)
	(begin
		(define j 1)

		(
			do ((i 1 (+ i 1))) ((> i (inexact->exact (* 0.5 NSTRIPE))))	       
			
			(define refevalname1 (string-append "RefVal_Nplus_" (number->string j) ) )
			(define placename1 (string-append "Place_Nplus_" (number->string j) ) )
			
			(define refevalname2 (string-append "RefVal_Nplus_" (number->string (+ j 1)) ) )
			(define placename2 (string-append "Place_Nplus_" (number->string (+ j 1)) ) )
			(set! j (+ j 2))
			
			(define defname "Nplus")
			(sdedr:define-gaussian-profile defname "PhosphorusActiveConcentration" "PeakPos" 0  "PeakVal" Nca0 "ValueAtDepth" Plow "Depth" Xj "Gauss"  "Factor" 0.8)

			(sdedr:define-refeval-window refevalname1 "Line"  
				(position (- 0 (+ LNplus_stripe (* 0.5 (- Lstripe_pitch LNplus_stripe)) (* (- i 1) Lstripe_pitch) ) ) 0 0) 
				(position (- 0 (+ (* 0.5 (- Lstripe_pitch LNplus_stripe)) (* (- i 1) Lstripe_pitch) ) ) 0 0) 
			)
;;			(sdedr:define-analytical-profile-placement placename1 defname refevalname1 "Both" "NoReplace" "Eval")

			(sdedr:define-refeval-window refevalname2 "Line"  
				(position (+ 0 (+ (* 0.5 (- Lstripe_pitch LNplus_stripe)) (* (- i 1) Lstripe_pitch) ) ) 0 0) 
				(position (+ 0 (+ LNplus_stripe (* 0.5 (- Lstripe_pitch LNplus_stripe)) (* (- i 1) Lstripe_pitch) ) ) 0 0) 
			)
;;			(sdedr:define-analytical-profile-placement placename2 defname refevalname2 "Both" "NoReplace" "Eval")
			
			(set! end-active (+ 0 (+ LNplus_stripe (* 0.5 (- Lstripe_pitch LNplus_stripe)) (* (- i 1) Lstripe_pitch) ) ) )
		)
	)
	(begin
			(define j 1)
			
			(define defname "Nplus")
			(sdedr:define-gaussian-profile defname "PhosphorusActiveConcentration" "PeakPos" 0  "PeakVal" Nca0 "ValueAtDepth" Plow "Depth" Xj "Gauss"  "Factor" 0.8)
			
			(sdedr:define-refeval-window "RefVal_Nplus_0" "Line"  
				(position (- 0 (* 0.5 LNplus_stripe)) 0 0) 
				(position (+ 0 (* 0.5 LNplus_stripe)) 0 0) 
			)
;;			(sdedr:define-analytical-profile-placement "Place_Nplus_0" defname "RefVal_Nplus_0" "Both" "NoReplace" "Eval")

		(
			do ((i 1 (+ i 1))) ((> i (inexact->exact (* 0.5 (- NSTRIPE 1)))))	       
			
			(define refevalname1 (string-append "RefVal_Nplus_" (number->string j) ) )
			(define placename1 (string-append "Place_Nplus_" (number->string j) ) )
			
			(define refevalname2 (string-append "RefVal_Nplus_" (number->string (+ j 1)) ) )
			(define placename2 (string-append "Place_Nplus_" (number->string (+ j 1)) ) )
			(set! j (+ j 2))
			

			(sdedr:define-refeval-window refevalname1 "Line"  
				(position (- (* i Lstripe_pitch) (* 0.5 LNplus_stripe) ) 0 0) 
				(position (+ (* i Lstripe_pitch) (* 0.5 LNplus_stripe) ) 0 0) 
			)
;;			(sdedr:define-analytical-profile-placement placename1 defname refevalname1 "Both" "NoReplace" "Eval")

			(sdedr:define-refeval-window refevalname2 "Line"  
				(position (- 0 (+ (* i Lstripe_pitch) (* 0.5 LNplus_stripe) )) 0 0) 
				(position (- 0 (- (* i Lstripe_pitch) (* 0.5 LNplus_stripe) )) 0 0) 
			)
;;			(sdedr:define-analytical-profile-placement placename2 defname refevalname2 "Both" "NoReplace" "Eval")
			
			(set! end-active (+ (* i Lstripe_pitch) (* 0.5 LNplus_stripe) ) )
		)
	)
)

(define Next 40)
(define Pext 10)

(define defname "Nplus")
(sdedr:define-gaussian-profile defname "PhosphorusActiveConcentration" "PeakPos" 0  "PeakVal" Nca "ValueAtDepth" Plow "Depth" Xj "Gauss"  "Factor" 0.8)

(sdedr:define-refeval-window "RefVal_Nplus" "Line"  
	(position (- (- 0 (* 0.5 (* NSTRIPE Lstripe_pitch))) Next) 0 0) 
	(position (+ (* 0.5 (* NSTRIPE Lstripe_pitch)) Next) 0 0) 
)
(sdedr:define-analytical-profile-placement "Place_Nplus" defname "RefVal_Nplus" "Both" "NoReplace" "Eval")

;;(set! end-active (+ 0 (+ LNplus_stripe (* 0.5 (- Lstripe_pitch LNplus_stripe)) (* (- i 1) Lstripe_pitch) ) ) )


(sdedr:define-refeval-window "Pplus"  "Line" 
         (position (+ Pext (- 0 (* 0.5 (* NSTRIPE Lstripe_pitch)))) (- Xj p_layer_Peak) 0) 
         (position (- (* 0.5 (* NSTRIPE Lstripe_pitch)) Pext) (- Xj p_layer_Peak) 0)
)
(sdedr:define-gaussian-profile "Pplus" "BoronActiveConcentration" "PeakPos" p_layer_Peak "PeakVal" p_well_doping_concentration "ValueAtDepth" (/ p_well_doping_concentration 10) "Depth" p_layer_Depth "Gauss" "Factor" 1)
(sdedr:define-analytical-profile-placement "Pplus" "Pplus" "Pplus" "Both" "NoReplace" "Eval")



(define listca (get-drs-list))

(define begin-GR (+ end-active gap))

		(
			do ((i 1 (+ i 1))) ((> i NGR))	       
			
			(define refevalname1 (string-append "RefVal_NGR_" (number->string j) ) )
			(define placename1 (string-append "Place_NGR_" (number->string j) ) )
			
			(define refevalname2 (string-append "RefVal_NGR_" (number->string (+ j 1)) ) )
			(define placename2 (string-append "Place_NGR_" (number->string (+ j 1)) ) )
			(set! j (+ j 2))
			
			(define defname "NGR")
			(sdedr:define-gaussian-profile defname "PhosphorusActiveConcentration" "PeakPos" 0  "PeakVal" Nca "ValueAtDepth" Plow "Depth" Xj "Gauss"  "Factor" 0.8)

			(sdedr:define-refeval-window refevalname1 "Line"  
				(position (- 0 (+ begin-GR LNplus_GR (* 0.5 (- LGR_pitch LNplus_GR)) (* (- i 1) LGR_pitch) ) ) 0 0) 
				(position (- 0 (+ begin-GR (* 0.5 (- LGR_pitch LNplus_GR)) (* (- i 1) LGR_pitch) ) ) 0 0) 
			)
			(sdedr:define-analytical-profile-placement placename1 defname refevalname1 "Both" "NoReplace" "Eval")

			(sdedr:define-refeval-window refevalname2 "Line"  
				(position (+ 0 (+ begin-GR (* 0.5 (- LGR_pitch LNplus_GR)) (* (- i 1) LGR_pitch) ) ) 0 0) 
				(position (+ 0 (+ begin-GR LNplus_GR (* 0.5 (- LGR_pitch LNplus_GR)) (* (- i 1) LGR_pitch) ) ) 0 0) 
			)
			(sdedr:define-analytical-profile-placement placename2 defname refevalname2 "Both" "NoReplace" "Eval")
			
		)

(define listdcathode (get-drs-list))

(sdedr:define-refeval-window "RefEval_anode" "Line"  (position (- 0 (+ L 10)) W 0) (position (+ L 10) W 0))

(sdedr:define-gaussian-profile "DopAnode" "BoronActiveConcentration" "PeakPos" 0  "PeakVal" Pan "ValueAtDepth" Plow "Depth" 5 "Gauss"  "Factor" 0.8)
(sdedr:define-analytical-profile-placement "Place_anode" "DopAnode" "RefEval_anode" "Both" "NoReplace" "Eval")

;;(sdedr:define-refeval-window "RefEval_pspray" "Line"  (position (- 0 (+ L 10)) 0 0) (position (+ L 10) 0 0))

;;(sdedr:define-gaussian-profile "Doppspray" "BoronActiveConcentration" "PeakPos" 0  "PeakVal" pspray "ValueAtDepth" Plow "Depth" 1 "Gauss"  "Factor" 0.8)
;;(sdedr:define-analytical-profile-placement "Place_pspray" "Doppspray" "RefEval_pspray" "Both" "NoReplace" "Eval")


(define Tox 0.1)
(sdegeo:create-rectangle (position (- 0 L) (- 0 Tox) 0) (position L 0 0) "Oxide" "Rox")

(define contlist (list))

	(define listbody listca)
	(for-each (lambda(body)
			(define start (edge:start (car (entity:edges body))))
			(define end (edge:end (car (entity:edges body))))
			(set! contlist (append contlist (list (position:x start))))
			(set! contlist (append contlist (list (position:x end))))
		)	
	listbody)
	
(set! contlist (sort < contlist))

(define cc 1)
(do ((i 0 (+ i 2))) ((> i (- (length contlist) 1) ))
    	       	   (if (eq? i 0)
		       	(begin

			(sdegeo:create-rectangle (position (list-ref contlist i) (- (- 0 Tox) 0) 0) (position (- (list-ref contlist (+ i 1)) 30) 0 0) "Aluminum" (string-append "RAl" (number->string cc)))
			(entity:delete (find-region-id (string-append "RAl" (number->string cc)) ))
			(sdegeo:set-default-boolean "BAB")
			(sdegeo:create-rectangle (position (- (list-ref contlist i) 5.0) (- 0 (+ Tox 0.1)) 0) (position (+ (list-ref contlist (+ i 1)) 5.0) 0 0) "Aluminum" (string-append "RAl" (number->string cc)))
			(sdegeo:define-contact-set (string-append "cathode" (number->string cc)) 4  (color:rgb 1 0 0 ) "##")
			(sdegeo:set-current-contact-set (string-append "cathode" (number->string cc)))
			(sdegeo:set-contact-boundary-edges (find-region-id (string-append "RAl" (number->string cc)) ))
			(entity:delete (find-region-id (string-append "RAl" (number->string cc)) ))
			(sdegeo:set-default-boolean "ABA")
			)
			(begin

			(if (eq? i (- (length contlist) 2))
		       	(begin
			(sdegeo:create-rectangle (position (+ (list-ref contlist i) 30) (- (- 0 Tox) 0) 0) (position (list-ref contlist (+ i 1)) 0 0) "Aluminum" (string-append "RAl" (number->string cc)))
			(entity:delete (find-region-id (string-append "RAl" (number->string cc)) ))
			(sdegeo:set-default-boolean "BAB")
			(sdegeo:create-rectangle (position (- (list-ref contlist i) 5.0) (- 0 (+ Tox 0.1)) 0) (position (+ (list-ref contlist (+ i 1)) 5.0) 0 0) "Aluminum" (string-append "RAl" (number->string cc)))
			(sdegeo:define-contact-set (string-append "cathode" (number->string cc)) 4  (color:rgb 1 0 0 ) "##")
			(sdegeo:set-current-contact-set (string-append "cathode" (number->string cc)))
			(sdegeo:set-contact-boundary-edges (find-region-id (string-append "RAl" (number->string cc)) ))
			(entity:delete (find-region-id (string-append "RAl" (number->string cc)) ))
			(sdegeo:set-default-boolean "ABA")
			)
)
			(begin
			(sdegeo:create-rectangle (position (list-ref contlist i) (- (- 0 Tox) al_dep) 0) (position (list-ref contlist (+ i 1)) (- 0 Tox) 0) "Aluminum" (string-append "RAl" (number->string cc)))
			(entity:delete (find-region-id (string-append "RAl" (number->string cc)) ))
			(sdegeo:set-default-boolean "BAB")
			(sdegeo:create-rectangle (position (- (list-ref contlist i) 5.0) (- 0 (+ Tox 0.1)) 0) (position (+ (list-ref contlist (+ i 1)) 5.0) 0 0) "Aluminum" (string-append "RAl" (number->string cc)))
			(sdegeo:define-contact-set (string-append "cathode" (number->string cc)) 4  (color:rgb 1 0 0 ) "##")
			(sdegeo:set-current-contact-set (string-append "cathode" (number->string cc)))
			(sdegeo:set-contact-boundary-edges (find-region-id (string-append "RAl" (number->string cc)) ))
			(entity:delete (find-region-id (string-append "RAl" (number->string cc)) ))
			(sdegeo:set-default-boolean "ABA")
			)
			)
			)

			(set! cc (+ cc 1))
)


	

(sdegeo:define-contact-set "anode" 4  (color:rgb 1 0 0 ) "##")

(sdegeo:set-current-contact-set "anode")
(sdegeo:set-contact-edges (find-edge-id (position 0 W 0)))






(define xcutlist (list))

	(define listbody listdcathode)
	(for-each (lambda(body)
			(define start (edge:start (car (entity:edges body))))
			(define end (edge:end (car (entity:edges body))))
			;;(define xpos (* 0.5 (+ (position:x (cdr oxbbox)) (position:x (car oxbbox)))))
			;;(set! xcutlist (append xcutlist (list xpos)))
			(set! xcutlist (append xcutlist (list (position:x start))))
			(set! xcutlist (append xcutlist (list (position:x end))))
		)	
	listbody)
(set! xcutlist (sort < xcutlist))
(define newlist (list))
(do ((i 1 (+ i 2))) ((> i (- (length xcutlist) 2) ))
			(set! newlist (append newlist (list (* 0.5 (+ (list-ref xcutlist i) (list-ref xcutlist (+ i 1)))))))	
)
(set! newlist (sort < (append newlist xcutlist)))
(sdeaxisaligned:set-parameters "xCuts" newlist  )
	



(sdedr:define-constant-profile "ConstantProfileDefinition_1" "BoronActiveConcentration" Plow)
(sdedr:define-constant-profile-material "ConstantProfilePlacement_1" "ConstantProfileDefinition_1" "Silicon")


(sdedr:define-refinement-size "RefinementDefinition_1" (* 0.1 L) (/ W 20.0) 1 0.05 0.05 0.05 )
(sdedr:define-refinement-placement "RefinementPlacement_1" "RefinementDefinition_1" (list "material" "Silicon" ) )
(sdedr:define-refinement-function "RefinementDefinition_1" "DopingConcentration" "MaxTransDiff" 1)



(define takemax (lambda(x y) (if (> (abs x) (abs y)) (define jj (abs x)) (define jj (abs y))) jj))

(define create-polygon-pos-list (lambda(name posx posy vv W L)
	(sdedr:define-refeval-window name "Polygon" (list 
		(position (- posx (* (gvector:y vv) W)) (+ posy (* (gvector:x vv) W)) 0)
		(position (+ posx (* (gvector:y vv) W)) (- posy (* (gvector:x vv) W)) 0)
		(position (+ (+ posx (* (gvector:x vv) L)) (* (gvector:y vv) W)) (- (+ posy (* (gvector:y vv) L)) (* (gvector:x vv) W)) 0)
		(position (- (+ posx (* (gvector:x vv) L)) (* (gvector:y vv) W)) (+ (+ posy (* (gvector:y vv) L)) (* (gvector:x vv) W)) 0)
	))
	
		(sdedr:define-refeval-window "Ref_fakeprofile" "Line"  
		(position (- posx (* (gvector:y vv) W)) (+ posy (* (gvector:x vv) W)) 0)
		(position (+ posx (* (gvector:y vv) W)) (- posy (* (gvector:x vv) W)) 0)
	)
		(sdedr:define-erf-profile "fakeprofile" "PMIUserField99" "SymPos" L  "MaxVal" 1 "ValueAtDepth" 0 "Depth" (+ L (* 0.5 W)) "Erf"  "Length" (* 0.5 W))
		(sdedr:define-analytical-profile-placement "Place_fakeprofile" "fakeprofile" "Ref_fakeprofile" "Both" "NoReplace" "Eval")

	(sdedr:define-refinement-size (string-append "DEF_" name) 
		(* 0.1 (takemax (* (gvector:y vv) W) (* (gvector:x vv) L)))
		(* 0.1 (takemax (* (gvector:x vv) W) (* (gvector:y vv) L)))
		1
		0.05
		0.05
		1
	)
	(sdedr:define-refinement-function (string-append "DEF_" name) "PMIUserField99" "MaxTransDiff" 0.1)
	(sdedr:define-refinement-placement (string-append "PLACE_" name) (string-append "DEF_" name) (list "window" name ) )

))

(create-polygon-pos-list "REFION" @posX@ 0 (gvector @dirX@ @dirY@ 0) whi Lhi)






(sde:build-mesh "snmesh" " " "n@node@_msh")







