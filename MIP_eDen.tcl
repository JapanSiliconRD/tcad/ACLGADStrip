#MIP_eDen.tcl
#MAKO: Output from Tcl commands history.

set node "n82"

load_file ${node}__0000_des.tdr
create_plot -dataset ${node}__0000_des
select_plots Plot_${node}__0000_des
set_plot_prop -plot Plot_${node}__0000_des -ratio_xtoy 100
zoom_plot -plot Plot_${node}__0000_des -window {-435.757 -0.983862 430.72 3.65078}
set_field_prop -plot Plot_${node}__0000_des -geom ${node}__0000_des Abs(eCurrentDensity-V) -show_bands
#-> 0
load_file ${node}__0001_des.tdr
create_plot -dataset ${node}__0001_des
select_plots Plot_${node}__0001_des
set_plot_prop -plot Plot_${node}__0001_des -ratio_xtoy 100
zoom_plot -plot Plot_${node}__0001_des -window {-435.757 -0.983862 430.72 3.65078}
set_field_prop -plot Plot_${node}__0001_des -geom ${node}__0001_des Abs(eCurrentDensity-V) -show_bands

load_file ${node}__0002_des.tdr
create_plot -dataset ${node}__0002_des
select_plots Plot_${node}__0002_des
set_plot_prop -plot Plot_${node}__0002_des -ratio_xtoy 100
zoom_plot -plot Plot_${node}__0002_des -window {-435.757 -0.983862 430.72 3.65078}
set_field_prop -plot Plot_${node}__0002_des -geom ${node}__0002_des Abs(eCurrentDensity-V) -show_bands

load_file ${node}__0003_des.tdr
create_plot -dataset ${node}__0003_des
select_plots Plot_${node}__0003_des
set_plot_prop -plot Plot_${node}__0003_des -ratio_xtoy 100
zoom_plot -plot Plot_${node}__0003_des -window {-435.757 -0.983862 430.72 3.65078}
set_field_prop -plot Plot_${node}__0003_des -geom ${node}__0003_des Abs(eCurrentDensity-V) -show_bands

load_file ${node}__0004_des.tdr
create_plot -dataset ${node}__0004_des
select_plots Plot_${node}__0004_des
set_plot_prop -plot Plot_${node}__0004_des -ratio_xtoy 100
zoom_plot -plot Plot_${node}__0004_des -window {-435.757 -0.983862 430.72 3.65078}
set_field_prop -plot Plot_${node}__0004_des -geom ${node}__0004_des Abs(eCurrentDensity-V) -show_bands
#-> Plot_${node}__0004_des
#-> Plot_${node}__0004_des
#-> ${node}__0004_des

