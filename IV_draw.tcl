#/home/kojin/work/Sentaurus/SimulationTCAD/2D/LGAD/LGADStrip/IV_draw.tcl
#MAKO: Output from Tcl commands history.

create_plot -1d -name Plot
select_plots Plot


set nodelist "n5 n85 n121 n35 n79 n115 n49 n73 n109 n21 n67 n103 n28 n61 n97 n42 n55 n91"
set labellist "ndep=0.8_n+=1e19_p+=3e16 ndep=0.8_n+=1e19_p+=1e16 ndep=0.8_n+=1e19_p+=5e16 ndep=0.8_n+=1e18_p+=3e16 ndep=0.8_n+=1e18_p+=1e16 ndep=0.8_n+=1e18_p+=5e16 ndep=0.8_n+=1e17_p+=3e16 ndep=0.8_n+=1e17_p+=1e16 ndep=0.8_n+=1e17_p+=5e16 ndep=0.3_n+=1e19_p+=3e16 ndep=0.3_n+=1e19_p+=1e16 ndep=0.3_n+=1e19_p+=5e16 ndep=0.3_n+=1e18_p+=3e16 ndep=0.3_n+=1e18_p+=1e16 ndep=0.3_n+=1e18_p+=5e16 ndep=0.3_n+=1e17_p+=3e16 ndep=0.3_n+=1e17_p+=1e16 ndep=0.3_n+=1e17_p+=5e16"
set i 0
foreach node $nodelist {
    puts "$node $i"
    load_file IV_${node}_des.plt
    create_curve -name Curve_$node -plot Plot -dataset IV_${node}_des -axisX {anode OuterVoltage} -axisY {anode TotalCurrent}
    set_curve_prop -plot Plot Curve_${node} -label [lindex $labellist $i]
    if {$i == 0} {
	set_plot_prop -plot Plot -hide_title
	set_axis_prop -plot Plot -axis y -type log
	set_legend_prop -plot Plot -position {0.14577 0.644481}
    }
    incr i
}



